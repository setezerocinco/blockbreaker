﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
	private Paddle paddle;
	private Rigidbody2D rb2d;
	private bool hasStarted = false;
	private Vector3 paddle2BallVector;

	// Use this for initialization
	void Start()
	{
		paddle = GameObject.FindObjectOfType<Paddle>();
		rb2d = GetComponent<Rigidbody2D>();
		paddle2BallVector = transform.position - paddle.transform.position;
	}

	// Update is called once per frame
	void Update()
	{
		if (!hasStarted)
		{
			// lock the ball to the paddle if the game has not yet started
			transform.position = paddle.transform.position + paddle2BallVector;

			if (Input.GetMouseButtonDown(0))
			{
				hasStarted = true; // release the ball from the paddle
				GetComponent<Rigidbody2D>().velocity = new Vector2(2f, 10f);
				//rb2d.AddForce(new Vector2(2f, 10f));
			}
		}
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		//Vector2 tweak = new Vector2(Random.Range(0f, 0.2f), Random.Range(0f, 0.2f));
		if (hasStarted)
		{
			GetComponent<AudioSource>().Play();
			GetComponent<Rigidbody2D>().velocity += new Vector2(Random.Range(0f, 0.2f), Random.Range(0f, 0.2f));
		}
	}
}
