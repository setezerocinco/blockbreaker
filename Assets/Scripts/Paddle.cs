﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
	public bool autoPlay;
	public float minX, maxX;

	private Ball ball;
	private Vector3 ballPos;
	private float mousePosInBlocks;
	private Vector3 paddlePos = new Vector3(0.5f, 0f, 0f);

	// Use this for initialization
	void Start()
	{
		ball = GameObject.FindObjectOfType<Ball>();
	}

	// Update is called once per frame
	void Update()
	{
		if (!autoPlay) MoveWithMouse();
		else AutoPlay();
	}

	void MoveWithMouse()
	{
		mousePosInBlocks = Input.mousePosition.x / Screen.width * 16;
		paddlePos = new Vector3(Mathf.Clamp(mousePosInBlocks, minX, maxX), transform.position.y, 0f);
		transform.position = paddlePos;
	}

	void AutoPlay()
	{
		paddlePos = new Vector3(0.5f, transform.position.y, 0f);
		ballPos = ball.transform.position;
		paddlePos.x = Mathf.Clamp(ballPos.x, minX, maxX);
		transform.position = paddlePos;
	}
}
