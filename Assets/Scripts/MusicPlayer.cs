﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
	static MusicPlayer mp = null;

	void Awake()
	{
		if (mp != null)
		{
			Destroy(gameObject);
			print("Duplicated self destructing.");
		}
		else
		{
			mp = this;
			GameObject.DontDestroyOnLoad(gameObject);
		}
	}

	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{

	}
}
