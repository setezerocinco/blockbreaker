﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
	public AudioClip crack;
	public GameObject smoke;
	public Sprite[] hitSprites;
	public static int breakableCount;

	private int timesHit;
	private bool isBreakable;
	private LevelManager levelManager;

	private void Awake()
	{
		breakableCount = 0;
	}

	// Use this for initialization
	void Start()
	{
		timesHit = 0;
		isBreakable = (tag == "Breakable");

		// keep track of breakable bricks
		if (isBreakable) breakableCount++;
		levelManager = GameObject.FindObjectOfType<LevelManager>();
	}

	// Update is called once per frame
	void Update()
	{
	}

	public void OnCollisionEnter2D(Collision2D collision)
	{
		AudioSource.PlayClipAtPoint(crack, transform.position);
		if (isBreakable) HandleHits();
	}

	public void HandleHits()
	{
		int maxHits = hitSprites.Length + 1;

		timesHit++;
		if (timesHit >= maxHits)
		{
			breakableCount--;
			levelManager.BrickDestroyed();
			PuffSmoke();
			Destroy(gameObject);
		}
		else LoadSprites();
	}

	void PuffSmoke()
	{
		GameObject smokePuff = Instantiate(smoke, transform.position, Quaternion.identity) as GameObject;
		var psMain = smokePuff.GetComponent<ParticleSystem>().main;
		psMain.startColor = gameObject.GetComponent<SpriteRenderer>().color;
	}

	private void LoadSprites()
	{
		int spriteIndex = timesHit - 1;
		if (hitSprites[spriteIndex] != null) GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
		else Debug.LogError("There is a Brick Sprite missing.");
	}
}
